package jephthia.jam.controllers;

import java.io.IOException;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.ResourceBundle;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.BorderPane;
import javafx.stage.Stage;
import jephthia.jam.crud.AppointmentGroupsIO;
import jephthia.jam.crud.AppointmentRecordsIO;
import jephthia.jam.crud.SmtpIO;
import jephthia.jam.entities.AppointmentGroup;
import jephthia.jam.entities.AppointmentRecord;
import jephthia.jam.entities.Smtp;

public class MainController
{
	private Logger log = LoggerFactory.getLogger(this.getClass().getName());
	
	@FXML private void handleNewAppointmentForm(ActionEvent event) throws SQLException
	{
		log.info("Handle New Appointment Form");
		
		ArrayList<AppointmentGroup> groups = new ArrayList<AppointmentGroup>();
		
		//Get a list of all the appointment groups so that the user
		//can choose which appointment group they want to set their
		//appointments to.
		
		try
		{
			AppointmentGroupsIO groupIO = new AppointmentGroupsIO();
			groups = groupIO.getAll();
		}
		catch (SQLException e)
		{
			e.printStackTrace();
		}		
		
		try
		{
		    // Instantiate the FXMLLoader
			FXMLLoader loader = new FXMLLoader();
			
			// Set the location of the fxml file in the FXMLLoader
			loader.setLocation(MainController.class.getResource("/fxml/AppointmentForm.fxml"));
			
			// Localize the loader with its bundle
			// Uses the default locale and if a matching bundle is not found
			// will then use MessagesBundle.properties
			loader.setResources(ResourceBundle.getBundle("MessagesBundle"));
			
			// Parent is the base class for all nodes that have children in the
			// scene graph such as AnchorPane and most other containers
			Parent parent = (AnchorPane) loader.load();
			
			// Load the parent into a Scene
			Scene scene = new Scene(parent);
			
			Stage stage = new Stage();
			stage.setTitle("New Appointment Record");
			
			// Put the Scene on Stage
			stage.setScene(scene);
			stage.show();
			
			AppointmentController controller = loader.getController();
			
			AppointmentRecordsIO recordsIO = new AppointmentRecordsIO();
			AppointmentRecord appointment = recordsIO.getByID(10);
			
			AppointmentGroupsIO groupIO = new AppointmentGroupsIO();
			AppointmentGroup appointmentGroup = groupIO.getRow(appointment.getAppointmentGroup());
			
			controller.setAppointmentRecord(appointment, appointmentGroup, groups);
			
			//((Node)(event.getSource())).getScene().getWindow().hide();
		} catch (IOException ex) {
		    log.error(null, ex);
		}
	}
	
	@FXML private void handleNewSmtpForm(ActionEvent event) throws SQLException
	{
		log.info("Handle New Smtp Form");
		
		try
		{
		    // Instantiate the FXMLLoader
			FXMLLoader loader = new FXMLLoader();
			
			// Set the location of the fxml file in the FXMLLoader
			loader.setLocation(MainController.class.getResource("/fxml/SmtpForm.fxml"));
			
			// Localize the loader with its bundle
			// Uses the default locale and if a matching bundle is not found
			// will then use MessagesBundle.properties
			loader.setResources(ResourceBundle.getBundle("MessagesBundle"));
			
			// Parent is the base class for all nodes that have children in the
			// scene graph such as AnchorPane and most other containers
			Parent parent = (AnchorPane) loader.load();
			
			// Load the parent into a Scene
			Scene scene = new Scene(parent);
			
			Stage stage = new Stage();
			stage.setTitle("New Smtp");
			
			// Put the Scene on Stage
			stage.setScene(scene);
			stage.show();
			
			SmtpController controller = loader.getController();
			SmtpIO smtpIO = new SmtpIO();
			Smtp smtp = smtpIO.getRow(1);
			
			controller.setSmtp(smtp);
			//((Node)(event.getSource())).getScene().getWindow().hide();
		} catch (IOException ex) {
		    log.error(null, ex);
		}
	}
	
	@FXML private void handleNewAppointmentGroupForm(ActionEvent event) throws SQLException
	{
		log.info("Handle New Appointment Group Form");
		
		try
		{
		    // Instantiate the FXMLLoader
			FXMLLoader loader = new FXMLLoader();
			
			// Set the location of the fxml file in the FXMLLoader
			loader.setLocation(MainController.class.getResource("/fxml/AppointmentGroupForm.fxml"));
			
			// Localize the loader with its bundle
			// Uses the default locale and if a matching bundle is not found
			// will then use MessagesBundle.properties
			loader.setResources(ResourceBundle.getBundle("MessagesBundle"));
			
			// Parent is the base class for all nodes that have children in the
			// scene graph such as AnchorPane and most other containers
			Parent parent = (AnchorPane) loader.load();
			
			// Load the parent into a Scene
			Scene scene = new Scene(parent);
			
			Stage stage = new Stage();
			stage.setTitle("New Appointment Group");
			
			// Put the Scene on Stage
			stage.setScene(scene);
			stage.show();
			
			AppointmentGroupController controller = loader.getController();
			
			AppointmentGroupsIO groupIO = new AppointmentGroupsIO();
			AppointmentGroup group = groupIO.getRow(50);
			
			controller.setAppointmentGroup(group);
			//((Node)(event.getSource())).getScene().getWindow().hide();
		} catch (IOException ex) {
		    log.error(null, ex);
		}
	}
	
	@FXML private void handleNewDatabaseForm(ActionEvent event) throws SQLException
	{
		log.info("Handle Database Form");
		
		try
		{
		    // Instantiate the FXMLLoader
			FXMLLoader loader = new FXMLLoader();
			
			// Set the location of the fxml file in the FXMLLoader
			loader.setLocation(MainController.class.getResource("/fxml/DbmsForm.fxml"));
			
			// Localize the loader with its bundle
			// Uses the default locale and if a matching bundle is not found
			// will then use MessagesBundle.properties
			loader.setResources(ResourceBundle.getBundle("MessagesBundle"));
			
			// Parent is the base class for all nodes that have children in the
			// scene graph such as AnchorPane and most other containers
			Parent parent = (AnchorPane) loader.load();
			
			// Load the parent into a Scene
			Scene scene = new Scene(parent);
			
			Stage stage = new Stage();
			stage.setTitle("New Database");
			
			// Put the Scene on Stage
			stage.setScene(scene);
			stage.show();

			//((Node)(event.getSource())).getScene().getWindow().hide();
		} catch (IOException ex) {
		    log.error(null, ex);
		}
	}
	
	@FXML private void handleOpenMonthlyCalendar(ActionEvent event) throws SQLException
	{
		log.info("Handle Open Monthly Calendar");
		
		try
		{
		    // Instantiate the FXMLLoader
			FXMLLoader loader = new FXMLLoader();
			
			// Set the location of the fxml file in the FXMLLoader
			loader.setLocation(MainController.class.getResource("/fxml/MonthlyCalendar.fxml"));
			
			// Localize the loader with its bundle
			// Uses the default locale and if a matching bundle is not found
			// will then use MessagesBundle.properties
			loader.setResources(ResourceBundle.getBundle("MessagesBundle"));
			
			// Parent is the base class for all nodes that have children in the
			// scene graph such as AnchorPane and most other containers
			Parent parent = (BorderPane) loader.load();
			
			// Load the parent into a Scene
			Scene scene = new Scene(parent);
			
			Stage stage = new Stage();
			stage.setTitle("Monthly Calendar");
			
			// Put the Scene on Stage
			stage.setScene(scene);
			stage.show();
			
			MonthlyCalendarController controller = loader.getController();
			controller.displayTable();
			
			//((Node)(event.getSource())).getScene().getWindow().hide();
		} catch (IOException ex) {
		    log.error(null, ex);
		}
	}
	
	@FXML private void handleOpenWeeklyCalendar(ActionEvent event)
	{
		log.info("Handle Open Weekly Calendar");
		
		try
		{
		    // Instantiate the FXMLLoader
			FXMLLoader loader = new FXMLLoader();
			
			// Set the location of the fxml file in the FXMLLoader
			loader.setLocation(MainController.class.getResource("/fxml/WeeklyCalendar.fxml"));
			
			// Localize the loader with its bundle
			// Uses the default locale and if a matching bundle is not found
			// will then use MessagesBundle.properties
			loader.setResources(ResourceBundle.getBundle("MessagesBundle"));
			
			// Parent is the base class for all nodes that have children in the
			// scene graph such as AnchorPane and most other containers
			Parent parent = (BorderPane) loader.load();
			
			// Load the parent into a Scene
			Scene scene = new Scene(parent);
			
			Stage stage = new Stage();
			stage.setTitle("Weekly Calendar");
			
			// Put the Scene on Stage
			stage.setScene(scene);
			stage.show();
			
			WeeklyCalendarController controller = loader.getController();
			
			//((Node)(event.getSource())).getScene().getWindow().hide();
		} catch (IOException ex) {
		    log.error(null, ex);
		}
	}
	
	@FXML private void handleOpenDailyCalendar(ActionEvent event)
	{
		log.info("Handle Open Daily Calendar");
		
		try
		{
		    // Instantiate the FXMLLoader
			FXMLLoader loader = new FXMLLoader();
			
			// Set the location of the fxml file in the FXMLLoader
			loader.setLocation(MainController.class.getResource("/fxml/DailyCalendar.fxml"));
			
			// Localize the loader with its bundle
			// Uses the default locale and if a matching bundle is not found
			// will then use MessagesBundle.properties
			loader.setResources(ResourceBundle.getBundle("MessagesBundle"));
			
			// Parent is the base class for all nodes that have children in the
			// scene graph such as AnchorPane and most other containers
			Parent parent = (BorderPane) loader.load();
			
			// Load the parent into a Scene
			Scene scene = new Scene(parent);
			
			Stage stage = new Stage();
			stage.setTitle("Daily Calendar");
			
			// Put the Scene on Stage
			stage.setScene(scene);
			stage.show();
			
			DailyCalendarController controller = loader.getController();
			
			//((Node)(event.getSource())).getScene().getWindow().hide();
		} catch (IOException ex) {
		    log.error(null, ex);
		}
	}
}
