package jephthia.jam.controllers;

import java.sql.SQLException;
import java.sql.Timestamp;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.ArrayList;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javafx.beans.binding.Bindings;
import javafx.beans.property.Property;
import javafx.beans.property.SimpleObjectProperty;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.Node;
import javafx.scene.control.CheckBox;
import javafx.scene.control.ChoiceBox;
import javafx.scene.control.DatePicker;
import javafx.scene.control.TextField;
import javafx.stage.Stage;
import jephthia.jam.crud.AppointmentRecordsIO;
import jephthia.jam.entities.AppointmentGroup;
import jephthia.jam.entities.AppointmentRecord;

public class AppointmentController
{
	private Logger log = LoggerFactory.getLogger(this.getClass().getName());
	private AppointmentRecord appointment;
	private AppointmentGroup appointmentGroup;
	private ArrayList<AppointmentGroup> groups;
	
    @FXML private TextField titleTextField;

    @FXML private TextField locationTextField;

    @FXML private TextField detailsTextField;

    @FXML private CheckBox alarmReminderCheckBox;

    @FXML private CheckBox wholeDayCheckBox;

    @FXML private ChoiceBox<String> appointmentGroupChoiceBox;
    
    @FXML private DatePicker startTimeDatePicker;

    @FXML private DatePicker endTimeDatePicker;

    @FXML private ChoiceBox<Integer> startTimeHoursChoiceBox;

    @FXML private ChoiceBox<Integer> startTimeMinutesChoiceBox;

    @FXML private ChoiceBox<Integer> endTimeHoursChoiceBox;

    @FXML private ChoiceBox<Integer> endTimeMinutesChoiceBox;
    
    /**
     * This method is automatically called after the fxml file has been loaded.
     * This code binds the properties of the data bean to the JavaFX controls.
     * Changes to a control is immediately written to the bean and a change to
     * the bean is immediately shown in the control.
     */
    @FXML private void initialize()
    {
    }
    
    /**
     * Sets all the instances that we need in order to properly show
     * our appointment form
     * @param appointment
     * @param appointmentGroup
     * @param groups Necessary to display a list of all the available groups to the user
     */
    public void setAppointmentRecord(AppointmentRecord appointment, AppointmentGroup appointmentGroup, ArrayList<AppointmentGroup> groups)
    {
    	this.groups = groups;
    	this.appointment = appointment;
    	this.appointmentGroup = appointmentGroup;
    	doBindings();
    }

    /**
     * Binds our fields to the bean so that the fields
     * can display the appropriate appointments' values.
     */
    private void doBindings()
    {
    	log.debug("Binding: AppointmentController");
    	
        Bindings.bindBidirectional(titleTextField.textProperty(), appointment.getTitleProperty());
        Bindings.bindBidirectional(locationTextField.textProperty(), appointment.getLocationProperty());
        Bindings.bindBidirectional(detailsTextField.textProperty(), appointment.getDetailsProperty());
        
        alarmReminderCheckBox.selectedProperty().bindBidirectional(appointment.isAlarmReminderProperty());
        wholeDayCheckBox.selectedProperty().bindBidirectional(appointment.isWholeDayProperty());
       
        for(AppointmentGroup group : groups)
        	appointmentGroupChoiceBox.getItems().add(group.getGroupNumber() + " " + group.getGroupName());

        appointmentGroupChoiceBox.getSelectionModel().select(appointmentGroup.getGroupNumber() + " " + appointmentGroup.getGroupName());
        
        startTimeHoursChoiceBox.getSelectionModel().select(new Integer(appointment.getStartTime().toLocalDateTime().getHour()));
        startTimeMinutesChoiceBox.getSelectionModel().select(new Integer(appointment.getStartTime().toLocalDateTime().getMinute()));
        
        endTimeHoursChoiceBox.getSelectionModel().select(appointment.getEndTime().toLocalDateTime().getHour());
        endTimeMinutesChoiceBox.getSelectionModel().select(appointment.getEndTime().toLocalDateTime().getMinute());

        Property<LocalDate> startTime = new SimpleObjectProperty<LocalDate>(appointment.getStartTime().toLocalDateTime().toLocalDate());
        startTimeDatePicker.valueProperty().bindBidirectional(startTime);
        
        Property<LocalDate> endTime = new SimpleObjectProperty<LocalDate>(appointment.getEndTime().toLocalDateTime().toLocalDate());
        endTimeDatePicker.valueProperty().bindBidirectional(endTime);
    }
    
    @FXML private void handleCreateAppointment(ActionEvent event)
    {
    	int groupNumber = Integer.parseInt(appointmentGroupChoiceBox.getSelectionModel().getSelectedItem().split(" ")[0]);
    	
    	LocalDateTime start = LocalDateTime.of(startTimeDatePicker.getValue().getYear(), startTimeDatePicker.getValue().getMonth(),
    											startTimeDatePicker.getValue().getDayOfMonth(),
    											startTimeHoursChoiceBox.getSelectionModel().getSelectedItem(),
    											startTimeMinutesChoiceBox.getSelectionModel().getSelectedItem());
    	
    	LocalDateTime end = LocalDateTime.of(endTimeDatePicker.getValue().getYear(), endTimeDatePicker.getValue().getMonth(),
												endTimeDatePicker.getValue().getDayOfMonth(),
												endTimeHoursChoiceBox.getSelectionModel().getSelectedItem(),
												endTimeMinutesChoiceBox.getSelectionModel().getSelectedItem());
    	
    	// Updating the start time, end time and appointment group just in case
    	// it was not properly bound
    	appointment.setAppointmentGroup(groupNumber);
    	appointment.setStartTime(Timestamp.valueOf(start));
    	appointment.setEndTime(Timestamp.valueOf(end));
    	
    	log.debug("RECORD: " + appointment.toString());
    	
    	try
    	{
    		AppointmentRecordsIO recordIO = new AppointmentRecordsIO();
			recordIO.addRow(this.appointment);
		}
    	catch (SQLException e)
    	{
			e.printStackTrace();
		}
    }
    
    @FXML private void handleEditAppointment(ActionEvent event)
    {
    	int groupNumber = Integer.parseInt(appointmentGroupChoiceBox.getSelectionModel().getSelectedItem().split(" ")[0]);
    	
    	LocalDateTime start = LocalDateTime.of(startTimeDatePicker.getValue().getYear(), startTimeDatePicker.getValue().getMonth(),
    											startTimeDatePicker.getValue().getDayOfMonth(),
    											startTimeHoursChoiceBox.getSelectionModel().getSelectedItem(),
    											startTimeMinutesChoiceBox.getSelectionModel().getSelectedItem());
    	
    	LocalDateTime end = LocalDateTime.of(endTimeDatePicker.getValue().getYear(), endTimeDatePicker.getValue().getMonth(),
												endTimeDatePicker.getValue().getDayOfMonth(),
												endTimeHoursChoiceBox.getSelectionModel().getSelectedItem(),
												endTimeMinutesChoiceBox.getSelectionModel().getSelectedItem());
    	
    	// Updating the start time, end time and appointment group just in case
    	// it was not properly bound
    	appointment.setAppointmentGroup(groupNumber);
    	appointment.setStartTime(Timestamp.valueOf(start));
    	appointment.setEndTime(Timestamp.valueOf(end));
    	
    	log.debug("RECORD: " + appointment.toString());
    	
    	try
    	{
    		AppointmentRecordsIO recordIO = new AppointmentRecordsIO();
			recordIO.updateRow(this.appointment);
		}
    	catch (SQLException e)
    	{
			e.printStackTrace();
		}
    }
    
    @FXML private void handleDeleteAppointment(ActionEvent event)
    {
    	try
    	{
    		AppointmentRecordsIO recordIO = new AppointmentRecordsIO();
			recordIO.deleteRow(this.appointment.getID());
			
			final Node source = (Node) event.getSource();
		    final Stage stage = (Stage) source.getScene().getWindow();
		    stage.close();
		}
    	catch (SQLException e)
    	{
			e.printStackTrace();
		}
    }
}
