package jephthia.jam.controllers;

import java.sql.SQLException;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javafx.beans.binding.Bindings;
import javafx.beans.property.ObjectProperty;
import javafx.beans.property.SimpleObjectProperty;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.Node;
import javafx.scene.control.ColorPicker;
import javafx.scene.control.TextField;
import javafx.scene.paint.Color;
import javafx.stage.Stage;
import jephthia.jam.crud.AppointmentGroupsIO;
import jephthia.jam.entities.AppointmentGroup;

public class AppointmentGroupController
{
	private Logger log = LoggerFactory.getLogger(this.getClass().getName());
	private AppointmentGroup group;
	
	@FXML private TextField nameTextField;
	@FXML private ColorPicker colorPicker;
	
	/**
	 * Set the appointment group that this form will be updating
	 * @param group
	 */
	public void setAppointmentGroup(AppointmentGroup group)
	{
		this.group = group;
		doBindings();
	}
	
	/**
	 * Binds the color and name of the appointment group
	 * to the fields
	 */
	private void doBindings()
	{
		Bindings.bindBidirectional(nameTextField.textProperty(), group.getGroupNameProperty());
		
		log.debug("CoLOR: " + group.getColor());
		
		Color c = Color.web(group.getColor(), 1.0);
		ObjectProperty<Color> colorProperty = new SimpleObjectProperty<>(c);
		Bindings.bindBidirectional(colorPicker.valueProperty(), colorProperty);
	}
	
	@FXML void handleCreateAppointmentGroup(ActionEvent event)
	{
		group.setColor(toRGBCode(colorPicker.getValue()));
		
		log.debug("GROUP: " + group.toString());
		
    	try
    	{
    		AppointmentGroupsIO groupIO = new AppointmentGroupsIO();
			groupIO.addRow(this.group);
		}
    	catch (SQLException e)
    	{
			e.printStackTrace();
		}
	}
	
	@FXML void handleEditAppointmentGroup(ActionEvent event)
	{
		group.setColor(toRGBCode(colorPicker.getValue()));
		
		log.debug("GROUP: " + group.getColor());
		
    	try
    	{
    		AppointmentGroupsIO groupIO = new AppointmentGroupsIO();
			groupIO.updateRow(this.group);
		}
    	catch (SQLException e)
    	{
			e.printStackTrace();
		}
	}
	
	@FXML void handleDeleteAppointmentGroup(ActionEvent event)
	{
    	try
    	{
    		AppointmentGroupsIO groupIO = new AppointmentGroupsIO();
			groupIO.deleteRow(this.group.getGroupNumber());
			
			// Close the window after we delete because the
			// user should not be able to continue updating
			// that appointment group
			
			final Node source = (Node) event.getSource();
		    final Stage stage = (Stage) source.getScene().getWindow();
		    stage.close();
		}
    	catch (SQLException e)
    	{
			e.printStackTrace();
		}
	}
	
	public static String toRGBCode(Color color )
    {
        return String.format( "#%02X%02X%02X",
            (int)( color.getRed() * 255 ),
            (int)( color.getGreen() * 255 ),
            (int)( color.getBlue() * 255 ) );
    }
}
