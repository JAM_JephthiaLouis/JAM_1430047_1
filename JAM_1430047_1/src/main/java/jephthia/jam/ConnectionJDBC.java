package jephthia.jam;
import static java.nio.file.Files.newInputStream;
import static java.nio.file.Paths.get;

import java.io.IOException;
import java.io.InputStream;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.StandardOpenOption;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.Properties;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * @author Jephthia Louis
 */
public class ConnectionJDBC
{
	private final Logger log = LoggerFactory.getLogger(getClass().getName());
	
	/**
	 * Used to get a connection to our database, using the correct credentials.
	 * @return The connection to the database
	 * @throws SQLException
	 */
    public Connection getConnection() throws SQLException
    {
    	Properties props = new Properties();
    	// The credentials are stored in a property file
    	Path file = get("src/main/resources", "DBMS.properties");
    	
    	if(Files.exists(file))
    	{
    		try(InputStream is = newInputStream(file, StandardOpenOption.CREATE))
    		{
    			props.load(is);
    		} catch (IOException e) {
				log.error("Couldn't read from the dbms properties file", e);
			}
    	}
    	
        String url = props.getProperty("DatabaseUrl");
        String user = props.getProperty("DatabaseUsername");
        String password = props.getProperty("DatabasePassword");
        
        return DriverManager.getConnection(url, user, password);
    }
}
