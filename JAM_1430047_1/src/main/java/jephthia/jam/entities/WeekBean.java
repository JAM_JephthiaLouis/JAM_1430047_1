package jephthia.jam.entities;

import javafx.beans.property.SimpleObjectProperty;
import javafx.scene.layout.VBox;

public class WeekBean
{
	private SimpleObjectProperty<VBox> sunday;
	private SimpleObjectProperty<VBox> monday;
	private SimpleObjectProperty<VBox> tuesday;
	private SimpleObjectProperty<VBox> wednesday;
	private SimpleObjectProperty<VBox> thursday;
	private SimpleObjectProperty<VBox> friday;
	private SimpleObjectProperty<VBox> saturday;
	
	public WeekBean()
	{
		this(new VBox(), new VBox(), new VBox(), new VBox(), new VBox(), new VBox(), new VBox());
	}
	
	public WeekBean(VBox sunday, VBox monday, VBox tuesday, VBox wednesday, VBox thursday, VBox friday, VBox saturday)
	{
		this.sunday = new SimpleObjectProperty<VBox>(sunday);
		this.monday = new SimpleObjectProperty<VBox>(monday);
		this.tuesday = new SimpleObjectProperty<VBox>(tuesday);
		this.wednesday = new SimpleObjectProperty<VBox>(wednesday);
		this.thursday = new SimpleObjectProperty<VBox>(thursday);
		this.friday = new SimpleObjectProperty<VBox>(friday);
		this.saturday = new SimpleObjectProperty<VBox>(saturday);
	}

	public VBox getSunday()
	{
		return sunday.get();
	}
	
	public SimpleObjectProperty<VBox> getSundayProperty()
	{
		return sunday;
	}
	
	public void setSunday(VBox sunday)
	{
		this.sunday.set(sunday);
	}
	
	public VBox getMonday()
	{
		return monday.get();
	}
	
	public SimpleObjectProperty<VBox> getMondayProperty()
	{
		return monday;
	}
	
	public void setMonday(VBox monday)
	{
		this.monday.set(monday);
	}
	
	public VBox getTuesday()
	{
		return tuesday.get();
	}
	
	public SimpleObjectProperty<VBox> getTuesdayProperty()
	{
		return tuesday;
	}
	
	public void setTuesday(VBox tuesday)
	{
		this.tuesday.set(tuesday);
	}
	
	public VBox getWednesday()
	{
		return wednesday.get();
	}
	
	public SimpleObjectProperty<VBox> getWednesdayProperty()
	{
		return wednesday;
	}
	
	public void setWednesday(VBox wednesday)
	{
		this.wednesday.set(wednesday);
	}
	
	public VBox getThursday()
	{
		return thursday.get();
	}
	
	public SimpleObjectProperty<VBox> getThursdayProperty()
	{
		return thursday;
	}
	
	public void setThursday(VBox thursday)
	{
		this.thursday.set(thursday);
	}
	
	public VBox getFriday()
	{
		return friday.get();
	}
	
	public SimpleObjectProperty<VBox> getFridayProperty()
	{
		return friday;
	}
	
	public void setFriday(VBox friday)
	{
		this.friday.set(friday);
	}
	
	public VBox getSaturday()
	{
		return saturday.get();
	}
	
	public SimpleObjectProperty<VBox> getSaturdayProperty()
	{
		return saturday;
	}
	
	public void setSaturday(VBox saturday)
	{
		this.saturday.set(saturday);
	}

	@Override
	public String toString() {
		return "WeekBean [sunday=" + sunday + ", monday=" + monday + ", tuesday=" + tuesday + ", wednesday=" + wednesday
				+ ", thursday=" + thursday + ", friday=" + friday + ", saturday=" + saturday + "]";
	}
}
