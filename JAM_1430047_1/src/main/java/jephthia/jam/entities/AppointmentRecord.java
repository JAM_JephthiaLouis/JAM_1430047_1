package jephthia.jam.entities;

import java.sql.Timestamp;
import java.time.LocalDateTime;

import javafx.beans.property.BooleanProperty;
import javafx.beans.property.IntegerProperty;
import javafx.beans.property.ObjectProperty;
import javafx.beans.property.SimpleBooleanProperty;
import javafx.beans.property.SimpleIntegerProperty;
import javafx.beans.property.SimpleObjectProperty;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;

/**
 * @author Jephthia Louis
 */
public class AppointmentRecord
{
    private IntegerProperty id;
    private StringProperty title;
    private StringProperty location;
    private ObjectProperty<Timestamp> startTime;
    private ObjectProperty<Timestamp> endTime;
    private StringProperty details;
    private BooleanProperty wholeDay;
    private IntegerProperty appointmentGroup;
    private BooleanProperty alarmReminder;
 
    public AppointmentRecord()
    {
    	this(0, "", "", Timestamp.valueOf(LocalDateTime.now()), Timestamp.valueOf(LocalDateTime.now()), "", false, 0, false);
    }
    
    public AppointmentRecord(int id, String title, String location, Timestamp startTime, Timestamp endTime, String details,
			boolean wholeDay, int appointmentGroup, boolean alarmReminder) {
		this.id = new SimpleIntegerProperty(id);
		this.title = new SimpleStringProperty(title);
		this.location = new SimpleStringProperty(location);
		this.startTime = new SimpleObjectProperty<Timestamp>(startTime);
		this.endTime = new SimpleObjectProperty<Timestamp>(endTime);
		this.details = new SimpleStringProperty(details);
		this.wholeDay = new SimpleBooleanProperty(wholeDay);
		this.appointmentGroup = new SimpleIntegerProperty(appointmentGroup);
		this.alarmReminder = new SimpleBooleanProperty(alarmReminder);
	}

	public int getID()
    {
        return id.get();
    }
	
	public IntegerProperty getIdProperty()
    {
    	return id;
    }

    public void setID(int id)
    {
        this.id = new SimpleIntegerProperty(id);
    }
    
    public String getTitle()
    {
        return title.get();
    }
    
    public StringProperty getTitleProperty()
    {
        return title;
    }

    public void setTitle(String title)
    {
        this.title = new SimpleStringProperty(title);
    }

    public String getLocation()
    {
        return location.get();
    }
    
    public StringProperty getLocationProperty()
    {
        return location;
    }

    public void setLocation(String location)
    {
        this.location = new SimpleStringProperty(location);
    }

    public Timestamp getStartTime()
    {
        return startTime.get();
    }
    
    public ObjectProperty<Timestamp> getStartTimeProperty()
    {
        return startTime;
    }

    public void setStartTime(Timestamp startTime)
    {
        this.startTime = new SimpleObjectProperty<Timestamp>(startTime);
    }

    public Timestamp getEndTime()
    {
        return endTime.get();
    }
    
    public ObjectProperty<Timestamp> getEndTimeProperty()
    {
        return endTime;
    }

    public void setEndTime(Timestamp endTime)
    {
        this.endTime = new SimpleObjectProperty<Timestamp>(endTime);
    }

    public String getDetails()
    {
        return details.get();
    }
    
    public StringProperty getDetailsProperty()
    {
        return details;
    }

    public void setDetails(String details)
    {
        this.details = new SimpleStringProperty(details);
    }

    public boolean isWholeDay()
    {
        return wholeDay.get();
    }
    
    public BooleanProperty isWholeDayProperty()
    {
        return wholeDay;
    }

    public void setWholeDay(boolean wholeDay)
    {
        this.wholeDay = new SimpleBooleanProperty(wholeDay);
    }

    public int getAppointmentGroup()
    {
        return appointmentGroup.get();
    }
    
    public IntegerProperty getAppointmentGroupProperty()
    {
        return appointmentGroup;
    }

    public void setAppointmentGroup(int appointmentGroup)
    {
        this.appointmentGroup = new SimpleIntegerProperty(appointmentGroup);
    }

    public boolean isAlarmReminder()
    {
        return alarmReminder.get();
    }
    
    public BooleanProperty isAlarmReminderProperty()
    {
        return alarmReminder;
    }

    public void setAlarmReminder(boolean alarmReminder)
    {
        this.alarmReminder = new SimpleBooleanProperty(alarmReminder);
    }

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + (isAlarmReminder() ? 1231 : 1237);
		result = prime * result + getAppointmentGroup();
		result = prime * result + ((getDetails() == null) ? 0 : getDetails().hashCode());
		result = prime * result + ((getEndTime() == null) ? 0 : getEndTime().hashCode());
		result = prime * result + getID();
		result = prime * result + ((getLocation() == null) ? 0 : getLocation().hashCode());
		result = prime * result + ((getStartTime() == null) ? 0 : getStartTime().hashCode());
		result = prime * result + ((getTitle() == null) ? 0 : getTitle().hashCode());
		result = prime * result + (isWholeDay() ? 1231 : 1237);
		return result;
	}

	@Override public boolean equals(Object obj)
	{
		if (this == obj)
			return true;
		
		if (obj == null)
			return false;
		
		if (getClass() != obj.getClass())
			return false;
		
		AppointmentRecord other = (AppointmentRecord) obj;
		
		if (isAlarmReminder() != other.isAlarmReminder())
			return false;
		
		if (getAppointmentGroup() != other.getAppointmentGroup())
			return false;
		
		if (getDetails() == null)
		{
			if (other.getDetails() != null)
				return false;
		}
		else if (!getDetails().equals(other.getDetails()))
			return false;
		
		if (getEndTime() == null)
		{
			if (other.getEndTime() != null)
				return false;
		}
		else if (!getEndTime().equals(other.getEndTime()))
			return false;
		
		if (getID() != other.getID())
			return false;
		
		if (getLocation() == null)
		{
			if (other.getLocation() != null)
				return false;
		}
		else if (!getLocation().equals(other.getLocation()))
			return false;
		
		if (getStartTime() == null)
		{
			if (other.getStartTime() != null)
				return false;
		}
		else if (!getStartTime().equals(other.getStartTime()))
			return false;
		
		if (getTitle() == null)
		{
			if (other.getTitle() != null)
				return false;
		}
		else if (!getTitle().equals(other.getTitle()))
			return false;
		
		if (isWholeDay() != other.isWholeDay())
			return false;
		
		return true;
	}

	@Override public String toString()
	{
		return "AppointmentRecord [id=" + getID() + ", title=" + getTitle() + ", location=" + getLocation() + ", startTime="
				+ getStartTime() + ", endTime=" + getEndTime() + ", details=" + getDetails() + ", wholeDay=" + isWholeDay()
				+ ", appointmentGroup=" + getAppointmentGroup() + ", alarmReminder=" + isAlarmReminder() + "]";
	}
}
