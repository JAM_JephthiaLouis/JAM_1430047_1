package jephthia.jam.entities;

import javafx.beans.property.IntegerProperty;
import javafx.beans.property.SimpleIntegerProperty;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;

/**
 * @author Jephthia Louis
 */
public class AppointmentGroup
{
    private IntegerProperty groupNumber;
    private StringProperty groupName;
    private StringProperty color;
   
    public AppointmentGroup()
    {
    	this(0, "", "");
    }
    
    public AppointmentGroup(int groupNumber, String groupName, String color)
    {
		this.groupNumber = new SimpleIntegerProperty(groupNumber);
		this.groupName = new SimpleStringProperty(groupName);
		this.color = new SimpleStringProperty(color);
	}

    public int getGroupNumber()
    {
        return groupNumber.get();
    }
    
    public IntegerProperty getGroupNumberProperty()
    {
        return groupNumber;
    }

    public void setGroupNumber(int groupNumber)
    {
        this.groupNumber = new SimpleIntegerProperty(groupNumber);
    }

    public String getGroupName()
    {
        return groupName.get();
    }
    
    public StringProperty getGroupNameProperty()
    {
        return groupName;
    }

    public void setGroupName(String groupName)
    {
        this.groupName = new SimpleStringProperty(groupName);
    }

    public String getColor()
    {
        return color.get();
    }
    
    public StringProperty getColorProperty()
    {
        return color;
    }

    public void setColor(String color)
    {
        this.color = new SimpleStringProperty(color);
    }

	@Override public int hashCode()
	{
		final int prime = 31;
		int result = 1;
		result = prime * result + ((getColor() == null) ? 0 : getColor().hashCode());
		result = prime * result + ((getGroupName() == null) ? 0 : getGroupName().hashCode());
		result = prime * result + getGroupNumber();
		return result;
	}

	@Override public boolean equals(Object obj)
	{
		if (this == obj)
			return true;
		
		if (obj == null)
			return false;
		
		if (getClass() != obj.getClass())
			return false;
		
		AppointmentGroup other = (AppointmentGroup) obj;
		
		if (getColor() == null)
		{
			if (other.getColor() != null)
				return false;
		}
		else if (!getColor().equals(other.getColor()))
			return false;
		
		if (getGroupName() == null)
		{
			if (other.getGroupName() != null)
				return false;
		}
		else if (!getGroupName().equals(other.getGroupName()))
			return false;
		
		if (getGroupNumber() != other.getGroupNumber())
			return false;
		
		return true;
	}

	@Override public String toString()
	{
		return "AppointmentGroups [groupNumber=" + getGroupNumber() + ", groupName=" + getGroupName()
				+ ", color=" + getColor() + "]";
	}
}
