package jephthia.jam;

import static java.nio.file.Paths.get;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.sql.SQLException;
import java.util.ResourceBundle;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.layout.AnchorPane;
import javafx.stage.Stage;
import jephthia.jam.controllers.DbmsController;
import jephthia.jam.email.EmailWatcher;

/**
 * @author Jephthia Louis
 */
public class MainAppFx extends Application
{
	private Logger log = LoggerFactory.getLogger(this.getClass().getName());
	
	// The primary window or frame of this application
    private Stage primaryStage;
    private EmailWatcher emailWatcher;
    private ScheduledExecutorService executor;
	
	@Override public void init()
	{
		// Initiate the task that will take care of sending the emails
		
		try {
			emailWatcher = new EmailWatcher();
		} catch (SQLException e) {
			e.printStackTrace();
		}
		// Multiple tasks can be scheduled, here we are scheduling just one
		executor = Executors.newScheduledThreadPool(1);
    }

	/**
	 * Setting up our main stage and our main scene
	 */
	@Override public void start(Stage primaryStage)
	{
		log.info("Program Begins");
		
		this.primaryStage = primaryStage;
		
		// The interval is between the start time of the task.
        // The task must complete its task before the next interval completes.
        // Runnable task, delay to start, interval between tasks, time units
        executor.scheduleWithFixedDelay(emailWatcher, 30, 30, TimeUnit.SECONDS);

		try
		{
			FXMLLoader mainLoader = new FXMLLoader();
			// Set the location of the fxml file in the FXMLLoader
    		mainLoader.setLocation(MainAppFx.class.getResource("/fxml/MainForm.fxml"));
			// Parent is the base class for all nodes that have children in the
			// scene graph such as AnchorPane and most other containers
			Parent parent = (AnchorPane) mainLoader.load();
			
			// Load the parent into a Scene
			Scene mainScene = new Scene(parent);
			
			Path file = get("src/main/resources", "DBMS.properties");
			
			// If the file with the database properties does not exists
			// we want to show the form to input the credentials first
	    	if(!Files.exists(file))
	    	{
			    // Instantiate the FXMLLoader
				FXMLLoader loader = new FXMLLoader();
				loader.setLocation(MainAppFx.class.getResource("/fxml/DbmsForm.fxml"));
				
				loader.setResources(ResourceBundle.getBundle("MessagesBundle"));
				
	    		Parent parent2 = (AnchorPane) loader.load();
				
				// Load the parent into a Scene
				Scene scene = new Scene(parent2);
	    		
	    		// Put the Scene on Stage
				primaryStage.setScene(scene);
				
				DbmsController controller = (DbmsController)loader.getController();
	    		controller.setStage(primaryStage, mainScene);
	    	}
	    	else
	    	{
	    		// Show our main scene
	    		primaryStage.setScene(mainScene);
	    	}
	
		} catch (IOException ex) {
		    log.error(null, ex);
		    System.exit(1);
		}
		
		// Set the window title
        primaryStage.setTitle("Calendar");
        // Raise the curtain on the Stage
        primaryStage.show();
	}

    public static void main(String[] args)
    {
    	Application.launch(args);
    }
}
