package jephthia.jam.email;

import java.sql.SQLException;
import java.sql.Timestamp;
import java.time.LocalDateTime;
import java.util.List;

import jephthia.jam.crud.AppointmentRecordsIO;
import jephthia.jam.crud.SmtpIO;
import jephthia.jam.entities.AppointmentRecord;
import jephthia.jam.entities.Smtp;
import jodd.mail.Email;
import jodd.mail.SendMailSession;
import jodd.mail.SmtpServer;
import jodd.mail.SmtpSslServer;

/**
 * @author Jephthia Louis
 */
public class EmailSender
{
	private Smtp smtp;
	
	public EmailSender() throws SQLException
	{
		this.smtp = getDefaultSmtp();
	}
	
	/**
	 * Returns the default smtp
	 * @return
	 * @throws SQLException
	 */
	public Smtp getDefaultSmtp() throws SQLException
    {
    	SmtpIO smtpIO = new SmtpIO();
    	
    	return smtpIO.getDefault();
    }
	
	/**
	 * Sends the test email reminders
	 * @throws SQLException
	 */
	public void sendReminders() throws SQLException
	{
		AppointmentRecordsIO recordsIO = new AppointmentRecordsIO();
		
		List<AppointmentRecord> appointments = findAppointments(recordsIO);
    	
    	for(AppointmentRecord appointment : appointments)
    	{
    		sendEmail(appointment);
    	}
	}
	
	/**
	 * Finds the appointments that we just created.
	 * @param recordsIO
	 * @return
	 * @throws SQLException
	 */
	private List<AppointmentRecord> findAppointments(AppointmentRecordsIO recordsIO) throws SQLException
    {
    	List<AppointmentRecord> appointments = recordsIO.getBetweenDates(Timestamp.valueOf(LocalDateTime.now().plusMinutes(120)),
    																	 Timestamp.valueOf(LocalDateTime.now().plusMinutes(123)));
    	
    	return appointments;
    }
	
	/**
	 * Send an email with the data of the specified
	 * appointment.
	 * @param appointment
	 */
	public void sendEmail(AppointmentRecord appointment)
	{

        // Create am SMTP server object
        SmtpServer<SmtpSslServer> smtpServer = SmtpSslServer
                .create(smtp.getServerUrl())
                .authenticateWith(smtp.getEmail(), smtp.getPassword());

        // Display Java Mail debug conversation with the server
        smtpServer.debug(true);

        // Using the fluent style of coding create a plain text message
        Email email = Email.create().from(smtp.getEmail())
                .to(smtp.getEmail())
                .subject(appointment.getTitle()).addText(appointment.getDetails() +
                		" Location: " + appointment.getLocation() + " Starts at: " + appointment.getStartTime()
                		+ " \nEnds at: " + appointment.getEndTime());

        // A session is the object responsible for communicating with the server
        SendMailSession session = smtpServer.createSession();

        // Like a file we open the session, send the message and close the
        // session
        session.open();
        session.sendMail(email);
        session.close();
    }
}
