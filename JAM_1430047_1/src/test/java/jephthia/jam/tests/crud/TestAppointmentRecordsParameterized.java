package jephthia.jam.tests.crud;

import static org.junit.Assert.assertEquals;

import java.sql.SQLException;
import java.util.Arrays;
import java.util.Collection;
import java.util.List;

import org.junit.Ignore;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;
import org.junit.runners.Parameterized.Parameters;

import jephthia.jam.crud.AppointmentRecordsIO;
import jephthia.jam.entities.AppointmentRecord;

/**
 * @author Jephthia Louis
 */
@Ignore
@RunWith(Parameterized.class)
public class TestAppointmentRecordsParameterized
{
	AppointmentRecord record;
	int expectedByDate;
	int expectedByTitle;
	int expectedBetweenDates;
	int expectedByMonth;
	int expectedByWeek;
	
	public TestAppointmentRecordsParameterized(AppointmentRecord record, int expectedByDate, int expectedByTitle,
			int expectedBetweenDates, int expectedByMonth, int expectedByWeek)
	{
		this.record = record;
		this.expectedByDate = expectedByDate;
		this.expectedByTitle = expectedByTitle;
		this.expectedBetweenDates = expectedBetweenDates;
		this.expectedByMonth = expectedByMonth;
		this.expectedByWeek = expectedByWeek;
	}
	
	@Parameters(name = "{index} plan[{0}]={1}]")
	public static Collection<Object[]> data() throws SQLException
	{   	
		AppointmentRecordsIO recordIO = new AppointmentRecordsIO();
	    	
		return Arrays.asList(new Object[][]{
			{recordIO.getByID(1), 1, 1, 25, 2, 1}
		});
	 }
	 
	@Test(timeout=1000)
	public void testGetByDate() throws SQLException
    {
    	AppointmentRecordsIO recordIO = new AppointmentRecordsIO();

    	List<AppointmentRecord> resultRecords = recordIO.getByDate(record.getStartTime());
    	
    	assertEquals(expectedByDate, resultRecords.size());
    }
	
	@Test(timeout=1000)
	public void testGetByTitle() throws SQLException
    {
    	AppointmentRecordsIO recordIO = new AppointmentRecordsIO();

    	List<AppointmentRecord> resultRecords = recordIO.getByTitle(record.getTitle());
    	
    	assertEquals(expectedByTitle, resultRecords.size());
    }
	
	@Test(timeout=1000)
	public void testGetBetweenDates() throws SQLException
    {
    	AppointmentRecordsIO recordIO = new AppointmentRecordsIO();

    	List<AppointmentRecord> resultRecords = recordIO.getBetweenDates(record.getStartTime(), record.getEndTime());
    	
    	assertEquals(expectedBetweenDates, resultRecords.size());
    }
	
	@Test(timeout=1000)
	public void testGetByMonth() throws SQLException
    {
    	AppointmentRecordsIO recordIO = new AppointmentRecordsIO();

    	List<AppointmentRecord> resultRecords = recordIO.getByMonth(record.getStartTime());
    	
    	assertEquals(expectedByMonth, resultRecords.size());
    }
	
	@Test(timeout=1000)
	public void testGetByWeek() throws SQLException
    {
    	AppointmentRecordsIO recordIO = new AppointmentRecordsIO();

    	List<AppointmentRecord> resultRecords = recordIO.getByWeek(record.getStartTime());
    	
    	assertEquals(expectedByWeek, resultRecords.size());
    }
}
