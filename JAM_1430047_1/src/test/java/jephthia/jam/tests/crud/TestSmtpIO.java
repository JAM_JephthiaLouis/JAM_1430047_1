package jephthia.jam.tests.crud;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.fail;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.Reader;
import java.io.StringReader;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.LinkedList;
import java.util.List;
import java.util.Scanner;

import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import jephthia.jam.crud.SmtpIO;
import jephthia.jam.entities.Smtp;

/**
 * @author Jephthia Louis
 */
//TODO use a random string generator to test the length of the field
//so that if we change the database's specs we can easily change the length size
@Ignore
public class TestSmtpIO
{

    // This is my local MySQL server
	private final String url = "jdbc:mysql://localhost:3306/JAMDB?autoReconnect=true&useSSL=false&noAccessToProcedureBodies=true"; 
    private final String user = "jephthia";
    private final String password = "database";

    private final Logger log = LoggerFactory.getLogger(this.getClass().getName());

    @Test(timeout=1000)
    public void testGetRow() throws SQLException
    {
    	Smtp expectedSmtp = new Smtp(1, "name1", "cdodshon0@webeden.co.uk", "GERM0kl", "randomURL1", 1927, 100, false);
    	
    	SmtpIO smtpIO = new SmtpIO();
    	
    	Smtp resultSmtp = smtpIO.getRow(1);
    	
    	assertEquals(expectedSmtp, resultSmtp);
    }
    
    @Test(timeout=1000)
    public void testUpdateRow() throws SQLException
    {
    	Smtp expectedSmtp = new Smtp(1, "testnewname", "testnewemail", "testnewpassword", "testnewserver", 1000, 110, false);
    	
    	SmtpIO smtpIO = new SmtpIO();
    	
    	Smtp smtpToUpdate = new Smtp(1, "testnewname", "testnewemail", "testnewpassword", "testnewserver", 1000, 110, false);
    	
    	smtpIO.updateRow(smtpToUpdate);
    	
    	Smtp resultSmtp = smtpIO.getRow(1);
    	
    	assertEquals(expectedSmtp, resultSmtp);
    }
    
    @Test(timeout=1000, expected=SQLException.class)
    public void testFailUpdateRow() throws SQLException
    {
    	SmtpIO smtpIO = new SmtpIO();
    	
    	Smtp smtpToUpdate = new Smtp(1, "testnewnhkkjjkhkhkjhkjhkjhkjhkjhkjhkjhkjhkkame", "testnewemail",
    								"testnewpassword", "testnewserver", 1000, 90, false);
    	
    	smtpIO.updateRow(smtpToUpdate);
    	
    	fail("We expected the update to throw an exception but it did not.");
    }
    
    @Test(timeout=1000)
    public void testDeleteRow() throws SQLException
    {
    	SmtpIO smtpIO = new SmtpIO();
    	
    	smtpIO.deleteRow(1);
    	
    	Smtp resultSmtp = smtpIO.getRow(1);
    	
    	assertNull(resultSmtp);
    }
    
    @Test(timeout=1000)
    public void testAddRow() throws SQLException
    {
    	SmtpIO smtpIO = new SmtpIO();
    	
    	Smtp smtpToAdd = new Smtp(-1, "newname", "newemail", "newpassword", "newserver", 1000, 90, false);
    	
    	int insertedID = smtpIO.addRow(smtpToAdd);
    	
    	Smtp expectedSmtp = new Smtp(insertedID, "newname", "newemail", "newpassword", "newserver", 1000, 90, false);
    	
    	Smtp resultSmtp = smtpIO.getRow(insertedID);
    	assertEquals(expectedSmtp, resultSmtp);
    }
    
    @Test(timeout=1000, expected=SQLException.class)
    public void testAddStringLength() throws SQLException
    {
    	SmtpIO smtpIO = new SmtpIO();
    	
    	Smtp smtpToAdd = new Smtp(-1, "newnjkhkjhkjhkjhkkjkhkjhkkkhjhjkame", "newemhhjkjkhjkhjhkhjjkhhjkjkhjkhail",
    								"newpassword", "newhbhjbbhjjbhjjkjkjkjhjhkjserver", 1000, 90, false);
    	
    	smtpIO.addRow(smtpToAdd);
    	
    	fail("We expected the add to throw an exception because the string's"
    			+ "length is too big but it did not.");
    }
    
    @Test(timeout=1000)
    public void testGetDefault() throws SQLException
    {
    	Smtp expectedSmtp = new Smtp(3, "Jephthia", "jam1430047@gmail.com", "whatever5", "smtp.gmail.com", 465, 120, true);
    	
    	SmtpIO smtpIO = new SmtpIO();
    	
    	Smtp resultSmtp = smtpIO.getDefault();
    	assertEquals(expectedSmtp, resultSmtp);
    }
    
    /**
     * This routine recreates the database before every test. This makes sure
     * that a destructive test will not interfere with any other test. Does not
     * support stored procedures.
     *
     * This routine is courtesy of Bartosz Majsak, the lead Arquillian developer
     * at JBoss
     */
    @Before public void seedDatabase()
    {
        log.info("Seeding Database");
        
        final String seedDataScript = loadAsString("CreateSmtpTable.sql");
        
        try (Connection connection = DriverManager.getConnection(url, user, password))
        {
            for (String statement : splitStatements(new StringReader(seedDataScript), ";"))
            {
                connection.prepareStatement(statement).execute();
            }
        } catch (SQLException e) {
            throw new RuntimeException("Failed seeding database", e);
        }
    }

    /**
     * The following methods support the seedDatabase method
     */
    private String loadAsString(final String path)
    {
        try (InputStream inputStream = Thread.currentThread().getContextClassLoader().getResourceAsStream(path);
             Scanner scanner = new Scanner(inputStream);)
        {
            return scanner.useDelimiter("\\A").next();
        } catch (IOException e) {
            throw new RuntimeException("Unable to close input stream.", e);
        }
    }

    private List<String> splitStatements(Reader reader, String statementDelimiter)
    {
        final BufferedReader bufferedReader = new BufferedReader(reader);
        final StringBuilder sqlStatement = new StringBuilder();
        final List<String> statements = new LinkedList<>();
        
        try
        {
            String line;
            
            while ((line = bufferedReader.readLine()) != null)
            {
                line = line.trim();
                if (line.isEmpty() || isComment(line))
                {
                    continue;
                }
                
                sqlStatement.append(line);
                
                if (line.endsWith(statementDelimiter))
                {
                    statements.add(sqlStatement.toString());
                    sqlStatement.setLength(0);
                }
            }
            
            return statements;
        } catch (IOException e) {
            throw new RuntimeException("Failed parsing sql", e);
        }
    }

    private boolean isComment(final String line)
    {
        return line.startsWith("--") || line.startsWith("//") || line.startsWith("/*");
    }
}
