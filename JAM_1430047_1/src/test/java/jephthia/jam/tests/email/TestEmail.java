package jephthia.jam.tests.email;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.Reader;
import java.io.StringReader;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.time.LocalDateTime;
import java.util.LinkedList;
import java.util.List;
import java.util.Scanner;

import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import jephthia.jam.crud.AppointmentRecordsIO;
import jephthia.jam.email.EmailSender;
import jephthia.jam.entities.AppointmentRecord;

/**
 * @author Jephthia Louis
 */
@Ignore
public class TestEmail
{
    // This is my local MySQL server
	private final String url = "jdbc:mysql://localhost:3306/JAMDB?autoReconnect=true&useSSL=false&noAccessToProcedureBodies=true"; 
    private final String user = "jephthia";
    private final String password = "database";

    private final Logger log = LoggerFactory.getLogger(this.getClass().getName());
	
    @Test(timeout=20000)
    public void testSendEmails() throws SQLException
    {
    	EmailSender sender = new EmailSender();
    	
    	AppointmentRecordsIO recordsIO = new AppointmentRecordsIO();
    	
    	createAppointments(recordsIO);
    	
    	sender.sendReminders();
    }
    
    private void createAppointments(AppointmentRecordsIO recordsIO) throws SQLException
    {
    	recordsIO.addRow(new AppointmentRecord(-1, "Dentist", "Downtown", Timestamp.valueOf(LocalDateTime.now().plusMinutes(120)), 
    			Timestamp.valueOf(LocalDateTime.now().plusMinutes(150)), "Dentist appointment, don't miss it.", false, 1, true));
    	
    	recordsIO.addRow(new AppointmentRecord(-1, "Meeting", "Laval", Timestamp.valueOf(LocalDateTime.now().plusMinutes(120)), 
    			Timestamp.valueOf(LocalDateTime.now().plusMinutes(143)), "Meeting, don't miss it.", false, 1, true));
    	
    	recordsIO.addRow(new AppointmentRecord(-1, "Doctor", "Montreal", Timestamp.valueOf(LocalDateTime.now().plusMinutes(120)), 
    			Timestamp.valueOf(LocalDateTime.now().plusMinutes(162)), "Doctor appointment, don't miss it.", false, 1, false));
    }
    
	/**
     * This routine recreates the database before every test. This makes sure
     * that a destructive test will not interfere with any other test. Does not
     * support stored procedures.
     *
     * This routine is courtesy of Bartosz Majsak, the lead Arquillian developer
     * at JBoss
     */
    @Before public void seedDatabase()
    {
        log.info("Seeding Database");
        
        final String seedDataScript = loadAsString("CreateSmtpTable.sql");
        
        try (Connection connection = DriverManager.getConnection(url, user, password))
        {
            for (String statement : splitStatements(new StringReader(seedDataScript), ";"))
            {
                connection.prepareStatement(statement).execute();
            }
        } catch (SQLException e) {
            throw new RuntimeException("Failed seeding database", e);
        }
    }

    /**
     * The following methods support the seedDatabase method
     */
    private String loadAsString(final String path)
    {
        try (InputStream inputStream = Thread.currentThread().getContextClassLoader().getResourceAsStream(path);
             Scanner scanner = new Scanner(inputStream);)
        {
            return scanner.useDelimiter("\\A").next();
        } catch (IOException e) {
            throw new RuntimeException("Unable to close input stream.", e);
        }
    }

    private List<String> splitStatements(Reader reader, String statementDelimiter)
    {
        final BufferedReader bufferedReader = new BufferedReader(reader);
        final StringBuilder sqlStatement = new StringBuilder();
        final List<String> statements = new LinkedList<>();
        
        try
        {
            String line;
            
            while ((line = bufferedReader.readLine()) != null)
            {
                line = line.trim();
                if (line.isEmpty() || isComment(line))
                {
                    continue;
                }
                
                sqlStatement.append(line);
                
                if (line.endsWith(statementDelimiter))
                {
                    statements.add(sqlStatement.toString());
                    sqlStatement.setLength(0);
                }
            }
            
            return statements;
        } catch (IOException e) {
            throw new RuntimeException("Failed parsing sql", e);
        }
    }

    private boolean isComment(final String line)
    {
        return line.startsWith("--") || line.startsWith("//") || line.startsWith("/*");
    }
}