package jephthia.jam.crud;

import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import jephthia.jam.ConnectionJDBC;
import jephthia.jam.entities.Smtp;

/**
 * @author Jephthia Louis
 */
public class SmtpIO
{
    private Logger log = LoggerFactory.getLogger(this.getClass().getName());

    /**
     * Updates the smtp object when found,
     * if no smtp object matches this one,
     * it will do nothing.
     * @param smtp
     * @throws SQLException
     */
    public void updateRow(Smtp smtp) throws SQLException
    {
    	log.debug("smpt updateRow called");
        
        String query = "{call smtp_update(?,?,?,?,?,?,?,?)}";
        
		try(Connection con = new ConnectionJDBC().getConnection();
			CallableStatement callSmtp = con.prepareCall(query))
		{		
			callSmtp.setInt(1, smtp.getId());
			callSmtp.setString(2, smtp.getName());
			callSmtp.setString(3, smtp.getEmail());
			callSmtp.setString(4, smtp.getPassword());
			callSmtp.setString(5, smtp.getServerUrl());
			callSmtp.setInt(6, smtp.getPort());
			callSmtp.setInt(7, smtp.getReminderInterval());
			callSmtp.setBoolean(8, smtp.isDefault());
			callSmtp.executeUpdate();
			
			log.debug("smpt row updated");
		} catch (SQLException e) {
			throw e;
		}
    }
    
    /**
     * Will add a new smtp object in the
     * database and the smtp object passed
     * will now be updated with the new id.
     * @param smtp
     * @return
     * @throws SQLException
     */
    public int addRow(Smtp smtp) throws SQLException
    {
    	log.debug("smpt addRow called");
        
    	int insertedID = 1;
    	
        String query = "{call smtp_add(?,?,?,?,?,?,?)}";
        
    	try(Connection con = new ConnectionJDBC().getConnection();
    		CallableStatement callSmtp = con.prepareCall(query))
    	{	
			callSmtp.setString(1, smtp.getName());
			callSmtp.setString(2, smtp.getEmail());
			callSmtp.setString(3, smtp.getPassword());
			callSmtp.setString(4, smtp.getServerUrl());
			callSmtp.setInt(5, smtp.getPort());
			callSmtp.setInt(6, smtp.getReminderInterval());
			callSmtp.setBoolean(7, smtp.isDefault());
			callSmtp.executeUpdate();
			
			log.debug("smtp row added");
			
			String lastID = "select last_insert_id()";
			
			PreparedStatement stmt = con.prepareStatement(lastID);
			ResultSet resultSet = stmt.executeQuery();
			
			if(resultSet.next())
				insertedID = resultSet.getInt("last_insert_id()");
			
			smtp.setId(insertedID);
		
    	} catch (SQLException e) {
			throw e;
		}
    	
    	return insertedID;
    }
    
    public void deleteRow(int id) throws SQLException
    {
        log.debug("smpt deleteRow called");
        
        String query = "{call smtp_delete(?)}";
        
		try(Connection con = new ConnectionJDBC().getConnection();
			CallableStatement callSmtp = con.prepareCall(query))
		{	
			callSmtp.setInt(1, id);
			callSmtp.executeUpdate();        
			
			log.debug("Row deleted");
		} catch (SQLException e) {
			throw e;
		}
    }
    
    public Smtp getRow(int id) throws SQLException
    {
        log.debug("smpt getRow called");
        
        String query = "select * from smtp where id = ?";
        
		try(Connection con = new ConnectionJDBC().getConnection();
			PreparedStatement stmt = con.prepareStatement(query))
		{	
			stmt.setInt(1, id);
			
			ResultSet results = stmt.executeQuery();
     
			if(results.next())
			{
				Smtp smtp = new Smtp();
				smtp.setId(results.getInt("id"));
				smtp.setName(results.getString("name"));
				smtp.setEmail(results.getString("email"));
				smtp.setPassword(results.getString("email_password"));
				smtp.setServerUrl(results.getString("server_url"));
				smtp.setPort(results.getInt("port"));
				smtp.setReminderInterval(results.getInt("reminder_interval"));
				smtp.setIsDefault(results.getBoolean("isDefault"));
			    
			    return smtp;
			}
		} catch (SQLException e) {
			throw e;
		}
        
        return null;
    }
    
    public Smtp getDefault() throws SQLException
    {
        log.debug("smpt getDefault called");
        
        String query = "select * from smtp where isDefault = true";
        
		try(Connection con = new ConnectionJDBC().getConnection();
			PreparedStatement stmt = con.prepareStatement(query))
		{		
			ResultSet results = stmt.executeQuery();
    
			if(results.next())
			{        
			    Smtp smtp = new Smtp();
				smtp.setId(results.getInt("id"));
				smtp.setName(results.getString("name"));
				smtp.setEmail(results.getString("email"));
				smtp.setPassword(results.getString("email_password"));
				smtp.setServerUrl(results.getString("server_url"));
				smtp.setPort(results.getInt("port"));
				smtp.setReminderInterval(results.getInt("reminder_interval"));
				smtp.setIsDefault(results.getBoolean("isDefault"));
				
			    return smtp;
			}
		} catch (SQLException e) {
			throw e;
		}
        
        return null;
    }
}
