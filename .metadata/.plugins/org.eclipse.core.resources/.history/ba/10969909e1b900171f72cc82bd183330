package jephthia.jam.controllers;

import static java.nio.file.Files.newInputStream;
import static java.nio.file.Files.newOutputStream;
import static java.nio.file.Paths.get;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.StandardOpenOption;
import java.sql.SQLException;
import java.util.Properties;
import java.util.ResourceBundle;

import javafx.application.Platform;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.TextField;
import javafx.scene.layout.AnchorPane;
import javafx.stage.Stage;
import jephthia.jam.MainAppFx;
import jephthia.jam.crud.AppointmentRecordsIO;

public class DbmsController
{
    @FXML private TextField urlTextField;

    @FXML private TextField dbnameTextField;

    @FXML private TextField usernameTextField;

    @FXML private TextField passwordTextField;

    @FXML private TextField portTextField;
    
    private Stage stage;
    
    public void setStage(Stage stage)
    {
    	this.stage = stage;
    }
    
    @FXML private void initialize() throws IOException
    {
    	Properties props = new Properties();
    	Path file = get("src/main/resources", "DBMS.properties");
    	
    	if(Files.exists(file))
    	{
    		try(InputStream is = newInputStream(file, StandardOpenOption.CREATE))
    		{
    			props.load(is);
    		}
    		
    		urlTextField.setText(props.getProperty("DatabaseUrl", ""));
    		dbnameTextField.setText(props.getProperty("DatabaseName", ""));
    		usernameTextField.setText(props.getProperty("DatabaseUsername", ""));
    		passwordTextField.setText(props.getProperty("DatabasePassword", ""));
    		portTextField.setText(props.getProperty("DatabasePort", ""));
    	}
    }

    @FXML private void handleSaveDbms(ActionEvent event) throws IOException
    {
    	Properties props = new Properties();
    	props.setProperty("DatabaseUrl", urlTextField.getText());
    	props.setProperty("DatabaseName", dbnameTextField.getText());
    	props.setProperty("DatabasePort", portTextField.getText());
    	props.setProperty("DatabaseUsername", usernameTextField.getText());
    	props.setProperty("DatabasePassword", passwordTextField.getText());
    	
    	Path file = get("src/main/resources", "DBMS.properties");
    	
    	try(OutputStream os = newOutputStream(file))
    	{
    		props.store(os, "DBMS settings");
    	}
    	
		try
		{
		    // Instantiate the FXMLLoader
			FXMLLoader loader = new FXMLLoader();

	    	// Set the location of the fxml file in the FXMLLoader
	    	loader.setLocation(MainAppFx.class.getResource("/fxml/MainForm.fxml"));
	    	
			// Localize the loader with its bundle
			// Uses the default locale and if a matching bundle is not found
			// will then use MessagesBundle.properties
			loader.setResources(ResourceBundle.getBundle("MessagesBundle"));
			
			// Parent is the base class for all nodes that have children in the
			// scene graph such as AnchorPane and most other containers
			Parent parent = (AnchorPane) loader.load();
			
			// Load the parent into a Scene
			Scene scene = new Scene(parent);
			
			// Put the Scene on Stage
			stage.setScene(scene);
		} catch (IOException ex) {
		    //errorAlert(ex.getMessage());
		    Platform.exit();
		}

    }
    
}