package jephthia.jam.email;

import java.sql.SQLException;
import java.sql.Timestamp;
import java.time.LocalDate;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javafx.application.Platform;
import javafx.scene.control.Alert;
import jephthia.jam.crud.AppointmentRecordsIO;
import jephthia.jam.entities.AppointmentRecord;

public class EmailWatcher implements Runnable
{
	private final Logger log = LoggerFactory.getLogger(getClass().getName());
	
	private EmailSender emailSender;
	private List<AppointmentRecord> todaysAppoinments;
	
	public EmailWatcher() throws SQLException
	{
		emailSender = new EmailSender();
		
		AppointmentRecordsIO recordIO = new AppointmentRecordsIO();
		recordIO.getByDate(Timestamp.valueOf(LocalDate.now().atStartOfDay()));
	}
	
	@Override public void run()
	{
		// Platform.runLater() allows this thread to interact with the main
        // JavaFX thread and therefore interact with controls.
        Platform.runLater(() -> {
            emailSender.sendEmail();

            log.info("Sent an email");

            Alert alert = new Alert(Alert.AlertType.INFORMATION);
            alert.setTitle("Sending An Email");
            alert.setHeaderText("Sending An Email.");
            alert.setContentText("An email has been sent.");
            alert.showAndWait();
        });
	}
}
