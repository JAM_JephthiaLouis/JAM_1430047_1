package jephthia.jam.controllers;

import java.sql.SQLException;
import java.sql.Timestamp;
import java.time.LocalDate;
import java.time.LocalTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.Label;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.layout.VBox;
import jephthia.jam.crud.AppointmentRecordsIO;
import jephthia.jam.entities.AppointmentRecord;
import jephthia.jam.entities.DayBean;

public class DailyCalendarController
{
	private Logger log = LoggerFactory.getLogger(getClass().getName());
	private int daysCounter = 0;
	
	private List<AppointmentRecord> currRecords;
	private LocalDate currDate;
	
    @FXML private Label dayLabel;
    @FXML private TableView<DayBean> dailyTableView;
    @FXML private TableColumn<DayBean, String> hoursCol;
    @FXML private TableColumn<DayBean, VBox> appointmentsCol;
    
    @FXML private void initialize()
    {
    	doBindings();
    	displayTable();
    }
    
    private void doBindings()
    {
    	hoursCol.setCellValueFactory(celldata -> celldata.getValue().getHourProperty());
    	appointmentsCol.setCellValueFactory(celldata -> celldata.getValue().getAppointmentsContainer());
    }
    
    private void displayTable()
    {
    	currDate = LocalDate.now().plusDays(daysCounter);
    	
    	dayLabel.setText(currDate.format(DateTimeFormatter.ofPattern("dd MMMM yyyy")));
    	
    	currRecords = getAppointments(currDate);
    	
    	dailyTableView.setItems(findHours());
    }
    
    private List<AppointmentRecord> getAppointments(LocalDate date)
    {
    	List<AppointmentRecord> records = new ArrayList<AppointmentRecord>();
    	AppointmentRecordsIO recordIO = new AppointmentRecordsIO();
    	
    	try {
			records = recordIO.getByDate(Timestamp.valueOf(date.atStartOfDay()));
		} catch (SQLException e) {
			e.printStackTrace();
		}
    	
    	return records;
    }
    
    private ObservableList<DayBean> findHours()
    {
    	ObservableList<DayBean> hours = FXCollections.observableArrayList();
    	
    	for(int i = 0; i < 24; i++)
    	{
    		VBox appointmentBox = new VBox();
    		VBox appointmentHalfBox = new VBox();
    		
    		LocalTime time = LocalTime.of(i, 0);
    		LocalTime halfTime = LocalTime.of(i, 30);
    		
    		for(AppointmentRecord record : currRecords)
    		{
    			Label appointmentLabel = new Label();
    			Label appointmentHalfLabel = new Label();
    			
    			if(record.getEndTime().toLocalDateTime().toLocalDate().isEqual(currDate.atStartOfDay().toLocalDate()))
    			{
    				int endHour = record.getEndTime().toLocalDateTime().getHour();
	    			int endMinutes = record.getEndTime().toLocalDateTime().getMinute();
	    			
	    			LocalTime currTime = LocalTime.of(endHour, endMinutes);
	    			
	    			if(time.getHour() == currTime.getHour())
	    			{
	    				if(currTime.getMinute() >= 30)
	    					appointmentLabel.setText(record.getTitle());
	
	    				appointmentHalfLabel.setText(record.getTitle());
	    			}
	    			else
	    			{
	    				if(time.isBefore(currTime))
	    					appointmentLabel.setText(record.getTitle());
	    				if(halfTime.isBefore(currTime))
	    					appointmentHalfLabel.setText(record.getTitle());
	    			}
    			}
    			else if(currDate.isAfter(record.getStartTime().toLocalDateTime().toLocalDate()))
    			{
    				appointmentLabel.setText(record.getTitle());
    				appointmentHalfLabel.setText(record.getTitle());
    			}
    			else
    			{
	    			int startHour = record.getStartTime().toLocalDateTime().getHour();
	    			int startMinutes = record.getStartTime().toLocalDateTime().getMinute();
	    			
	    			LocalTime currTime = LocalTime.of(startHour, startMinutes);
	    			
	    			if(time.getHour() == currTime.getHour())
	    			{
	    				if(currTime.getMinute() < 30)
	    					appointmentLabel.setText(record.getTitle());
	
	    				appointmentHalfLabel.setText(record.getTitle());
	    			}
	    			else
	    			{
	    				if(time.isAfter(currTime))
	    					appointmentLabel.setText(record.getTitle());
	    				if(halfTime.isAfter(currTime))
	    					appointmentHalfLabel.setText(record.getTitle());
	    			}
    			}
    			
    			appointmentBox.getChildren().add(appointmentLabel);
    			appointmentHalfBox.getChildren().add(appointmentHalfLabel);
    		}
   
    		DayBean dayBean = new DayBean();
    		dayBean.setHour(i + ":" + "00");	
    		dayBean.setAppointmentsContainer(appointmentBox);
    		hours.add(dayBean);
    		
    		DayBean dayBeanHalf = new DayBean();
    		dayBeanHalf.setHour(i + ":" + "30");
    		dayBeanHalf.setAppointments(appointmentHalfBox);
    		hours.add(dayBeanHalf);
    	}
    	
    	return hours;
    }
    
    @FXML private void onNextDay(ActionEvent event)
    {
    	daysCounter++;
    	displayTable();
    }

    @FXML private void onPreviousDay(ActionEvent event)
    {
    	daysCounter--;
    	displayTable();
    }
}
