package jephthia.jam.controllers;

import java.sql.SQLException;
import java.sql.Timestamp;
import java.time.LocalDate;
import java.time.LocalTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.Label;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import jephthia.jam.crud.AppointmentRecordsIO;
import jephthia.jam.entities.AppointmentRecord;
import jephthia.jam.entities.DayBean;

public class DailyCalendarController
{
	private Logger log = LoggerFactory.getLogger(getClass().getName());
	private int daysCounter = 0;
	
	private List<AppointmentRecord> currRecords;
	
    @FXML private Label dayLabel;
    @FXML private TableView<DayBean> dailyTableView;
    @FXML private TableColumn<DayBean, String> hoursCol;
    @FXML private TableColumn<DayBean, String> appointmentsCol;
    
    @FXML private void initialize()
    {
    	doBindings();
    	displayTable();
    }
    
    private void doBindings()
    {
    	hoursCol.setCellValueFactory(celldata -> celldata.getValue().getHourProperty());
    	appointmentsCol.setCellValueFactory(celldata -> celldata.getValue().getAppointmentsProperty());
    }
    
    private void displayTable()
    {
    	LocalDate currDate = LocalDate.now().plusDays(daysCounter);
    	
    	dayLabel.setText(currDate.format(DateTimeFormatter.ofPattern("dd MMMM yyyy")));
    	
    	currRecords = getAppointments(currDate);
    	
    	for(AppointmentRecord record : records)
    	{
    		
    	}
    	
    	dailyTableView.setItems(findHours());
    }
    
    private List<AppointmentRecord> getAppointments(LocalDate date)
    {
    	List<AppointmentRecord> records = new ArrayList<AppointmentRecord>();
    	AppointmentRecordsIO recordIO = new AppointmentRecordsIO();
    	
    	try {
			records = recordIO.getByDate(Timestamp.valueOf(date.atStartOfDay()));
		} catch (SQLException e) {
			e.printStackTrace();
		}
    	
    	return records;
    }
    
    private ObservableList<DayBean> findHours()
    {
    	ObservableList<DayBean> hours = FXCollections.observableArrayList();
    	
    	for(int i = 0; i < 24; i++)
    	{	
    		String appointmentTitles = "";
    		String appointmentHalfTitles = "";
    		
    		LocalTime time = LocalTime.of(i, 0);
    		LocalTime halfTime = LocalTime.of(i, 30);
    		
    		for(AppointmentRecord record : currRecords)
    		{
    			int startHour = record.getStartTime().toLocalDateTime().getHour();
    			int startMinutes = record.getStartTime().toLocalDateTime().getMinute();
    			
    			LocalTime currTime = LocalTime.of(startHour, startMinutes);
    			
    			if(time.isAfter(currTime))
    				appointmentTitles += record.getTitle() + ", ";
    			else if(halfTime.isAfter(currTime))
    				appointmentHalfTitles += record.getTitle() + ", ";
    		}
   
    		DayBean dayBean = new DayBean();
    		dayBean.setHour(i + ":" + "00");
    		dayBean.setAppointments(appointmentTitles);
    		hours.add(dayBean);
    		
    		DayBean dayBeanHalf = new DayBean();
    		dayBeanHalf.setHour(i + ":" + "30");
    		dayBeanHalf.setAppointments(appointmentHalfTitles);
    		hours.add(dayBeanHalf);
    	}
    	
    	return hours;
    }
    
    @FXML private void onNextDay(ActionEvent event)
    {
    	daysCounter++;
    	displayTable();
    }

    @FXML private void onPreviousDay(ActionEvent event)
    {
    	daysCounter--;
    	displayTable();
    }
}
